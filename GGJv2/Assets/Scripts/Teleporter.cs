﻿using UnityEngine;
using System.Collections;

/*
* Teleports Collider2D objects (with "Player" tag) that go through the trigger to objectToTeleportTo.
*/
public class Teleporter : MonoBehaviour
{

		// > 0 if the colliding GameObject came from the right, < 0 from the left.
		private int enterOrientation;
		
		// Flag to prevent bugs while flipping the character inside the collider.
		private bool insideTrigger;
		
		public GameObject objectToTeleportTo;
		
		/*
		* Detects from which side the collisioning object came from.
		*/		
		void OnTriggerEnter2D (Collider2D c)
		{
				if (insideTrigger)
						return;
				insideTrigger = true;
				
				if (c.tag == "Player") {
						float xDif = c.gameObject.transform.position.x - gameObject.transform.position.x;
						enterOrientation = xDif > 0 ? 1 : -1;
				}
		}
		
		/*
		* If the collisioning object goes through the trigger, teleports it to the objectToTeleportTo location.
		*/
		void OnTriggerExit2D (Collider2D c)
		{
				int exitOrientation = 0;
				float xDif = 0f;
				if (c.tag == "Player") {
						xDif = c.gameObject.transform.position.x - gameObject.transform.position.x;
						exitOrientation = xDif > 0 ? 1 : -1;
				}
				
				// if it leaves to a different place than where it came from
				if (exitOrientation != enterOrientation) {
						// teleport it
						Vector2 newPos = objectToTeleportTo.transform.position;
						newPos.x += xDif;
						newPos.y = c.gameObject.transform.position.y;
						c.gameObject.transform.position = newPos;
				}
				
				insideTrigger = false;
		}
	
}
